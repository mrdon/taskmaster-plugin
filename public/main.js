// @todo
//  - don't submit empty list
//  - dnd sorting
//  - close dialog and auto refresh issue on submit?

(function (window) {
  var AP = window.RA || window.AP;
  // keep submit disabled until the form has submittable data
  var submitButton = AP.Dialog.getButton("submit");
  submitButton.disable();

  AJS.$(function ($) {

    var Model = Backbone.Model, Collection = Backbone.Collection, View = Backbone.View;

    var bus = _.extend({}, Backbone.Events), on = _.bind(bus.on, bus), trigger = _.bind(bus.trigger, bus);

    var Subtask = Model;

    var Subtasks = Collection.extend({
      initialize: function () {
        _.bindAll(this);
      }
    });

    var SubtaskView = View.extend({
      template: "subtask-content",
      tagName: "li",
      events: {
        "change .subtask-title": "change",
        "keydown .subtask-title": "keydown",
        "click .subtask-title": "focus",
        "focus .subtask-title": "focused",
        "blur .subtask-title": "blurred",
        "click .subtask-remove": "remove"
      },
      initialize: function () {
        _.bindAll(this);
        this.render();
        this.$title = this.$el.find(".subtask-title");
        this.$el.data("view", this);
      },
      render: function () {
        this.$el.html(this.template ? AJS.template.load(this.template).fill(this.model.toJSON()) : "");
      },
      getTitle: function () {
        return $.trim(this.$title.val() || "");
      },
      setTitle: function (title) {
        this.$title.val(title);
      },
      isTitleEmpty: function () {
        return this.getTitle().length === 0;
      },
      change: function () {
        this.model.set("title", this.getTitle());
      },
      focus: function () {
        this.$title.focus().select();
      },
      focused: function () {
        this.edit();
      },
      blurred: function () {
        this.commit(false);
      },
      edit: function () {
        this.$el.removeClass("committed");
        this.ensureVisible();
      },
      ensureVisible: function () {
        var top;
        if (this.$el.is(":first-child")) {
          top = 0;
        }
        else if (this.$el.is(":last-child")) {
          top = $(document).height();
        }
        else {
          top = this.$el.offset().top - 5;
        }
        $("html, body").scrollTop(top);
      },
      commit: function (focus, insert, before) {
        if (!this.isTitleEmpty()) {
          this.$el.addClass("committed");
          trigger("commit", focus, insert && this.model, before);
        }
      },
      remove: function () {
        if (this.$el.is(":last-child")) {
          this.setTitle("");
        }
        else {
          trigger("remove", this.model);
          this.$el.slideUp(100, function () {
            var next = $(this).next().data("view");
            if (next) next.focus();
            $(this).remove();
          });
        }
      },
      keydown: function (e) {
        var code = e.which;
        if (code === 13 || code === 38 || code === 40 || (code === 68 && e.ctrlKey)) {
          e.preventDefault();
          if (code === 13) { // enter
            var insert = (!e.shiftKey && !e.ctrlKey) || e.shiftKey;
            var before = e.shiftKey;
            this.commit(true, insert, before);
          }
          if (code === 38) { // up arrow
            var $prev = this.$el.prev("li");
            if ($prev.length === 1) {
              this.commit(false);
              $prev.data("view").focus();
            }
          }
          if (code === 40) { // down arrow
            var $next = this.$el.next("li");
            if ($next.length === 1) {
              this.commit(false);
              $next.data("view").focus();
            }
          }
          if (code === 68 && e.ctrlKey) { // ctrl+d
            this.remove();
          }
        }
      }
    });

    var SubtasksView = View.extend({
      el: "#subtasks",
      initialize: function () {
        _.bindAll(this);
        on("commit", this.subtaskCommit);
        on("remove", this.subtaskRemove);
        this.collection = new Subtasks();
        this.render();
        this.addNew(true);
        submitButton.bind(this.submit);
      },
      render: function () {
        var $el = this.$el;
        $el.empty();
        this.collection.each(function (subtask) {
          $el.append(new SubtaskView({model: subtask}));
        });
      },
      subtaskCommit: function (focusNext, relativeTo, before) {
        if (relativeTo) {
          var index = this.collection.indexOf(relativeTo);
          var next = this.collection.at(index + 1);
          if (!before && next && !next.get("title")) {
            this.$el.find("li:nth-child(" + (index + 2) + ")").data("view").focus();
          }
          else {
            this.addNew(focusNext, index, before);
          }
        }
        else {
          var last = this.$el.find("li").last().data("view");
          if (last.isTitleEmpty()) {
            if (focusNext) last.focus();
          }
          else {
            this.addNew(focusNext);
          }
        }
        if (this.collection.length > 1) {
          submitButton.enable();
        }
      },
      subtaskRemove: function (subtask) {
        var index = this.collection.indexOf(subtask) + 1;
        var $next = this.$el.find("li:nth-child(" + (index + 1) + ")");
        var view = $next.length > 0 ? $next.data("view") : this.getLastView();
        view.focus();
        view.ensureVisible();
        this.collection.remove(subtask);
        if (this.collection.length === 1) {
          submitButton.disable();
        }
      },
      addNew: function (focus, at, before) {
        var subtask = new Subtask;
        at = _.isNumber(at) ? at : this.collection.length;
        var view = new SubtaskView({model: subtask});
        if (this.collection.length === 0) {
          this.$el.append(view.el);
          this.collection.add(subtask);
        }
        else {
          if (!before) at += 1;
          var $li = this.$el.find("li:nth-child(" + at + ")");
          if ($li.length === 0) this.$el[!before ? "append" : "prepend"](view.el);
          else $li.after(view.el);
          this.collection.add(subtask, {at: at });
        }
        if (focus) view.focus();
      },
      getLastView: function () {
        return this.$el.find("li").last().data("view");
      },
      show: function () {
        this.$el.show();
      },
      hide: function () {
        this.$el.hide();
      },
      submit: function () {
        var tasks = [];
        this.collection.each(function (subtask) {
          var title = subtask.get("title");
          if (title) tasks.push(title);
        });
        var parentIssueKey = $("#parentIssueKey").val();
        var localBaseUrl = AP.localUrl();
        var self = this;
        self.hide();
        messagesView.hide();
        waitView.show();
        submitButton.disable();
        $.ajax({
          url: localBaseUrl + "/create",
          type: "post",
// @todo need to get encrypted session cookie from a meta here instead of auth state
//          headers: {"AP-Auth-State": AJS.$('meta[name=ap-auth-state]').attr("content")},
          data: {
            tasks: tasks.join("\n"),
            parentIssueKey: parentIssueKey
          },
          success: function () {
            waitView.hide();
            var rnd = Math.floor(Math.random() * 10e12).toString(16);
            messagesView.showSuccess("Sub-tasks created", "<a href='?refresh=" + rnd + "'>Refresh Issue</a>");
          },
          error: function (xhr) {
            submitButton.enable();
            waitView.hide();
            var err,
              body = xhr.responseText,
              status = xhr.statusText;
            if (body) {
              var contentType = xhr.getResponseHeader("Content-Type");
              if (contentType && contentType.indexOf("application/json") === 0) {
                var json = JSON.parse(body);
                err = json.error ? json.error : body;
              }
              else {
                err = body;
              }
            }
            else {
              err = status ? status : "Cause unknown";
            }
            messagesView.showError("Error creating sub-tasks", err);
            self.show();
          }
        });
        return false;
      }
    });

    var WaitView = View.extend({
      el: "#wait",
      initialize: function () {
        this.spinner = new Spinner({
          lines: 13,
          length: 7,
          width: 5,
          radius: 10,
          corners: 1,
          rotate: 0,
          color: "#000",
          speed: 1,
          trail: 60,
          shadow: false,
          hwaccel: false,
          className: "spinner",
          zIndex: 2e9,
          top: "auto",
          left: "auto"
        }).spin(this.el);
      },
      show: function () {
        this.$el.removeClass("hidden");
      },
      hide: function () {
        this.$el.addClass("hidden");
      }
    });

    var MessagesView = View.extend({
      el: "#aui-message-bar",
      show: function () {
        this.$el.show();
      },
      hide: function () {
        this.$el.hide();
      },
      clear: function () {
        this.$el.empty();
      },
      showSuccess: function (title, body) {
        this.showMessage("success", title, body, false);
      },
      showError: function (title, body) {
        this.showMessage("error", title, body, true);
      },
      showMessage: function (type, title, body, closeable) {
        this.clear();
        this.show();
        AJS.messages[type]({
          id: type + "-message",
          title: title,
          body: body,
          closeable: !!closeable
        });
      }
    });

    var waitView = new WaitView();
    var messagesView = new MessagesView();
    var subtasksView = new SubtasksView();

  });

})(window);

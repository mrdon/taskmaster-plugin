module.exports = function (app, plugin, root) {

  app.get(root + "/dialog",

    plugin.authenticate(),

    function (req, res) {
      res.render("dialog", {
        parentIssueKey: req.param("issue_key")
      });
    }
  );

};

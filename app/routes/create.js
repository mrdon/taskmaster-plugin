module.exports = function (app, plugin, root) {

  app.post(root + "/create",

    plugin.authenticate(),

    function (req, res) {
      var issues = require("../jira/issues")(plugin.httpClient(req));
      var issueKey = req.param("parentIssueKey");
      var tasks = req.param("tasks").split("\n");
      function onError(err) {
        console.log(err.msg + (err.detail ? ": " + err.detail : ""));
        res.send(500, {error: err.toString()});
      }
      issues.getIssue(issueKey).then(
        function (issue) {
          var projectKey = issue.fields.project.key;
          issues.getSubtaskIssueType(projectKey).then(
            function (issueType) {
              issues.createSubtasks(projectKey, issueKey, issueType.id, tasks).then(
                function (results) {
                  res.send(results.map(function (result) {
                    return result.key;
                  }));
                },
                onError
              );
            },
            onError
          );
        },
        onError
      );
    }

  );

};

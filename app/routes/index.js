var fs = require("fs");

module.exports = function (app, plugin) {

  var root = "/atlassian-labs-taskmaster-plugin";

  // redirect root
  app.get(root, function (req, res) {
    var descriptor = root + "/atlassian-plugin.xml";
    res.format({
      "application/xml": function () {
        res.redirect(descriptor);
      },
      "text/html": function () {
        res.redirect(plugin.descriptor.documentationUrl() || descriptor);
      }
    });
  });

  // load all route controllers defined in this dir
  fs.readdirSync(__dirname).forEach(function (file) {
    if (file !== "index.js") require("./" + file)(app, plugin, root);
  });

};

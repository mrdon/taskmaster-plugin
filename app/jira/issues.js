var _ = require("underscore");
var Q = require("q");

function Issues(http) {
  this.http = http;
}

var proto = Issues.prototype;

proto.getIssue = function (issueKey) {
  return invoke(this.http, "get", {
    uri: "/rest/api/2/issue/" + issueKey
  });
};

proto.createSubtasks = function (projectKey, parentIssueKey, issueTypeId, summaries) {
  var self = this;
  return Q.all(summaries.map(function (summary) {
    return self.createSubtask(projectKey, parentIssueKey, issueTypeId, summary);
  }));
};

proto.createSubtask = function (projectKey, parentIssueKey, issueTypeId, summary) {
  return invoke(this.http, "post", {
    uri: "/rest/api/2/issue",
    body: {
      fields: {
        summary: summary,
        project: {key: projectKey},
        parent: {key: parentIssueKey},
        issuetype: {id: issueTypeId}
      }
    }
  });
};

proto.getIssueCreateMeta = function (projectKey) {
  return invoke(this.http, "get", {
    uri: "/rest/api/2/issue/createmeta?projectKeys=" + projectKey
  });
};

proto.getSubtaskIssueType = function (projectKey) {
  var dfd = Q.defer();
  this.getIssueCreateMeta(projectKey).then(
    function (meta) {
      var issuetype = _(meta.projects[0].issuetypes).find(function (issuetype) {
        return !!issuetype.subtask;
      });
      if (issuetype) {
        dfd.resolve(issuetype);
      }
      else {
        dfd.reject(new Error("No subtask issue type found in project: " + projectKey));
      }
    },
    dfd.reject
  );
  return dfd.promise;
};

function invoke(http, method, options) {
  options.json = true;
  var dfd = Q.defer();
  http[method](options, function (err, response) {
    var code = response.statusCode;
    if (err) {
      dfd.reject(err);
    }
    else if (code < 200 || code >= 300) {
      var msg = "Unexpected response: " + response.statusCode;
      var ex = new Error(msg);
      ex.detail = response.body;
      dfd.reject(ex);
    }
    else {
      dfd.resolve(response.body, response);
    }
  });
  return dfd.promise;
}

module.exports = function (http) {
  return new Issues(http);
};

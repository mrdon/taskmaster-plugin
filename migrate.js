var pg = require("pg");
var Q = require("q");

var properties = {
  OAUTH_HOST_PUBLIC_KEY: "publicKey",
  HOST_BASE_URL: "hostBaseUrl",
  HOST_PRODUCT_TYPE: "productType"
};

function migrate(plugin, next) {

  var url = plugin.config.store().connection;

  function exec(stmt, vars, callback) {
    if (typeof vars === "function") {
      callback = vars;
      vars = [];
    }
    pg.connect(url, function (err, client, done) {
      if (err) {
        callback(err);
        done();
      }
      else {
        client.query(stmt, vars, function (err, results) {
          callback(err, results);
          done();
        });
      }
    });
  }

  exec("SELECT * FROM plugin_settings;", function (err, results) {
    if (err) return next(err);
    var clients = {};

    results.rows.forEach(function (row) {
      var key = row.key;
      var val = row.value;
      if (key && key.indexOf("atlassian-labs-taskmaster-plugin.") === 0) {
        var parts = key.split(".");
        var property = properties[parts[1]] || parts[1];
        var clientKey = parts[2];
        if (clientKey) {
          var client = clients[clientKey] = clients[clientKey] || {};
          client[property] = val;
        }
      }
    });

    //noinspection JSValidateTypes
    Q.all(Object.keys(clients).map(function (clientKey) {
      var client = clients[clientKey];
      var dfd = Q.defer();
      plugin.settings.hget(clientKey).then(
        function (settings) {
          if (!settings) {
            plugin.settings.hset(clientKey, client).then(
              function () {
                plugin.logger.warn("Migrated " + clientKey + " plugin settings.");
                dfd.resolve(client);
              },
              dfd.reject
            );
          }
          else {
            dfd.resolve(client);
          }
        },
        dfd.reject
      );
      return dfd.promise;
    })).then(
      function () { next(); },
      next
    );
  });

}

module.exports = migrate;

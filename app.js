var express = require("express");
var ap3 = require("ap3");
var http = require("http");
var paths = require("path");
var urls = require("url");
var routes = require("./app/routes");
var migrate = require("./migrate");

var app = express();
var plugin = ap3(app);

var port = plugin.config.port();
var devMode = app.get("env") == "development";
var localResourceUrl = urls.format(urls.parse(plugin.config.localBaseUrl()));

app.set("port", port);
app.set("views", __dirname + "/app/views");
app.set("view engine", "jade");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser());
app.use(express.cookieSession({
  key: "session",
  secret: plugin.config.secret()
}));
// @todo middleware to handle auth state header or param
app.use(plugin.middleware());
app.use(resourceHelper());
app.use(app.router);
app.use(express.static(paths.join(__dirname, "public")));

if (devMode) {
  app.use(express.errorHandler());
}

routes(app, plugin);

http.createServer(app).listen(port, function () {
  plugin.logger.info("Plugin server listening on port " + port + ".");
  if (devMode) {
    plugin.register();
  }
});

function resourceHelper() {
  var localBaseUrl = plugin.config.localBaseUrl();
  var parsed = urls.parse(localBaseUrl);
  delete parsed.pathname;
  var resourceBaseUrl = urls.format(parsed);
  return function (req, res, next) {
    res.locals.resource = function (path) {
      return resourceBaseUrl + path;
    };
    next();
  };
}

// migrate(plugin, function (err) {
//   if (err) {
//     plugin.logger.error(err);
//     process.exit(1);
//   }
//   else {
//     plugin.logger.info("Migrated legacy plugin settings.");
//     start();
//   }
// });

// function start() {
//   http.createServer(app).listen(port, function () {
//     plugin.logger.info("Plugin server listening on port " + port + ".");
//     if (devMode) {
//       plugin.register(plugin);
//     }
//   });
// }
